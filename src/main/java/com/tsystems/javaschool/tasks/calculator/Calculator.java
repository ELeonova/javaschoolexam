package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            if (statement == null){
                return null;
            }
            char[] tokens = statement.toCharArray();


            Stack<Float> values = new Stack<Float>();


            Stack<Character> ops = new Stack<Character>();


            for (int i = 0; i < tokens.length; i++) {


                if (tokens[i] >= '0' && tokens[i] <= '9') {
                    StringBuffer sbuf = new StringBuffer();

                    while (i < tokens.length && (tokens[i] >= '0' && tokens[i] <= '9' || tokens[i] == '.'))
                        sbuf.append(tokens[i++]);
                    values.push(Float.parseFloat(sbuf.toString()));
                    i--;
                } else if (tokens[i] == '(')
                    ops.push(tokens[i]);


                else if (tokens[i] == ')') {
                    while (ops.peek() != '(')
                        values.push(applyOp(ops.pop(), values.pop(), values.pop()));
                    ops.pop();
                } else if (tokens[i] == '+' || tokens[i] == '-' ||
                        tokens[i] == '*' || tokens[i] == '/') {
                    while (!ops.empty() && hasPrecedence(tokens[i], ops.peek()))
                        values.push(applyOp(ops.pop(), values.pop(), values.pop()));


                    ops.push(tokens[i]);
                } else {
                    return null;
                }
            }


            while (!ops.empty())
                values.push(applyOp(ops.pop(), values.pop(), values.pop()));


            DecimalFormat df = new DecimalFormat("0.####");


            return (df.format(values.pop())).replace(",", ".");

        } catch (EmptyStackException | NumberFormatException | UnsupportedOperationException e) {
            return null;
        }


    }


    public static boolean hasPrecedence(char op1, char op2) {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
            return false;
        else
            return true;
    }


    public static float applyOp(char op, float b, float a) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0)
                    throw new
                            UnsupportedOperationException();
                return a / b;
        }
        return 0;

    }

}
