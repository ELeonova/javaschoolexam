package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            List<Integer> numbers = inputNumbers;

            if (numbers.contains(null)) {
                throw new CannotBuildPyramidException();
            }
            Collections.sort(numbers);
            int len = numbers.size();
            int sum = 0;
            int line_counter = 1;
            while (sum < len) {
                sum += line_counter;
                line_counter++;
            }
            if (sum != len) {
                throw new CannotBuildPyramidException();
            }

            line_counter--;


            int column_couner = line_counter * 2 - 1;
            int[][] result = new int[line_counter][column_couner];
            int start_j = line_counter - 1;
            int i = 0;
            while (i != len) {
                for (int c = 0; c < line_counter; c++) {
                    start_j -= c;
                    for (int p = 0; p <= c; p++) {
                        result[c][start_j + p * 2] = numbers.get(i);
                        i++;

                    }
                    start_j = line_counter - 1;
                }
            }


            return result;

        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
    }


}
