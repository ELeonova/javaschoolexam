package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try{
            List input1 = x;
            List input2 = y;
            if(input2 == null) {throw new IllegalArgumentException();}
            int[] arr = new int[input1.size()];
            int index = 0;
            int temp = 0;

            for (int i = 0; i < input1.size(); i++) {

                input2 = input2.subList(temp, input2.size());
                temp = input2.indexOf(input1.get(i));
                if (temp == -1) {
                    return false;
                } else {
                    arr[i] = index + temp;
                    index = index + temp;

                }

            }

            return true;

        }catch(NullPointerException e){
            throw new IllegalArgumentException();

        }
    }
}
